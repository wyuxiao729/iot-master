package com.wisdom.collect.modbus.facade;

import com.wisdom.collect.modbus.Modbus;
import com.wisdom.collect.modbus.io.AbstractModbusTransport;
import com.wisdom.collect.modbus.io.ModbusTCPTransaction;
import com.wisdom.collect.modbus.net.TCPMasterConnection;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ModbusTCPMaster extends AbstractModbusMaster {

    private TCPMasterConnection connection;
    private boolean reconnecting = false;
    private boolean useRtuOverTcp = false;

    public ModbusTCPMaster(String addr) {
        this(addr, Modbus.DEFAULT_PORT, Modbus.DEFAULT_TIMEOUT, false, false);
    }

    public ModbusTCPMaster(String addr, boolean useRtuOverTcp) {
        this(addr, Modbus.DEFAULT_PORT, Modbus.DEFAULT_TIMEOUT, false, useRtuOverTcp);
    }

    public ModbusTCPMaster(String addr, int port) {
        this(addr, port, Modbus.DEFAULT_TIMEOUT, false, false);
    }

    public ModbusTCPMaster(String addr, int port, boolean useRtuOverTcp) {
        this(addr, port, Modbus.DEFAULT_TIMEOUT, false, useRtuOverTcp);
    }

    public ModbusTCPMaster(String addr, int port, int timeout, boolean reconnect) {
        this(addr, port, timeout, reconnect, false);
    }

    public ModbusTCPMaster(String addr, int port, int timeout, boolean reconnect, boolean useRtuOverTcp) {
        super();
        this.useRtuOverTcp = useRtuOverTcp;
        try {
            InetAddress slaveAddress = InetAddress.getByName(addr);
            connection = new TCPMasterConnection(slaveAddress);
            connection.setPort(port);
            connection.setTimeout(timeout);
            this.timeout = timeout;
            setReconnecting(reconnect);
        } catch (UnknownHostException e) {
            throw new RuntimeException("Failed to contruct ModbusTCPMaster instance.", e);
        }
    }

    @Override
    public void connect() throws Exception {
        if (connection != null && !connection.isConnected()) {
            connection.connect(useRtuOverTcp);
            transaction = connection.getModbusTransport().createTransaction();
            ((ModbusTCPTransaction) transaction).setReconnecting(reconnecting);
            setTransaction(transaction);
        }
    }

    @Override
    public void disconnect() {
        if (connection != null && connection.isConnected()) {
            connection.close();
            transaction = null;
            setTransaction(null);
        }
    }


    public boolean isReconnecting() {
        return reconnecting;
    }

    public synchronized void setReconnecting(boolean b) {
        reconnecting = b;
        if (transaction != null) {
            ((ModbusTCPTransaction) transaction).setReconnecting(b);
        }
    }

    @Override
    public void setTimeout(int timeout) {
        super.setTimeout(timeout);
        if (connection != null) {
            connection.setTimeout(timeout);
        }
    }

    @Override
    public AbstractModbusTransport getTransport() {
        return connection == null ? null : connection.getModbusTransport();
    }
}