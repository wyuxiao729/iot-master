package com.wisdom.collect.modbus.io;


import com.wisdom.collect.modbus.Modbus;
import com.wisdom.collect.modbus.ModbusIOException;
import com.wisdom.collect.modbus.msg.ModbusRequest;
import com.wisdom.collect.modbus.msg.ModbusResponse;
import com.wisdom.collect.modbus.net.AbstractModbusListener;

import java.io.IOException;

public abstract class AbstractModbusTransport {

    protected int timeout = Modbus.DEFAULT_TIMEOUT;


    public void setTimeout(int time) {
        timeout = time;
    }


    public abstract void close() throws IOException;

    public abstract ModbusTransaction createTransaction();

    public abstract void writeRequest(ModbusRequest msg) throws ModbusIOException;

    public abstract void writeResponse(ModbusResponse msg) throws ModbusIOException;


    public abstract ModbusRequest readRequest(AbstractModbusListener listener) throws ModbusIOException;

    public abstract ModbusResponse readResponse() throws ModbusIOException;

}