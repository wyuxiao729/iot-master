package com.wisdom.collect.modbus.io;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;

public class BytesInputStream
        extends FastByteArrayInputStream implements DataInput {

    DataInputStream dataInputStream;

    public BytesInputStream(int size) {
        super(new byte[size]);
        dataInputStream = new DataInputStream(this);
    }

    public BytesInputStream(byte[] data) {
        super(data);
        dataInputStream = new DataInputStream(this);
    }


    public void reset(byte[] data) {
        pos = 0;
        mark = 0;
        buf = data;
        count = data.length;
    }


    public void reset(byte[] data, int length) {
        pos = 0;
        mark = 0;
        count = length;
        buf = data;
    }


    public void reset(int length) {
        pos = 0;
        count = length;
    }


    public int skip(int n) {
        mark(pos);
        pos += n;
        return n;
    }


    public synchronized byte[] getBuffer() {
        byte[] dest = new byte[buf.length];
        System.arraycopy(buf, 0, dest, 0, dest.length);
        return dest;
    }

    @Override
    public int getBufferLength() {
        return buf.length;
    }

    @Override
    public void readFully(byte b[]) throws IOException {
        dataInputStream.readFully(b);
    }

    @Override
    public void readFully(byte b[], int off, int len) throws IOException {
        dataInputStream.readFully(b, off, len);
    }

    @Override
    public int skipBytes(int n) throws IOException {
        return dataInputStream.skipBytes(n);
    }

    @Override
    public boolean readBoolean() throws IOException {
        return dataInputStream.readBoolean();
    }

    @Override
    public byte readByte() throws IOException {
        return dataInputStream.readByte();
    }

    @Override
    public int readUnsignedByte() throws IOException {
        return dataInputStream.readUnsignedByte();
    }

    @Override
    public short readShort() throws IOException {
        return dataInputStream.readShort();
    }

    @Override
    public int readUnsignedShort() throws IOException {
        return dataInputStream.readUnsignedShort();
    }

    @Override
    public char readChar() throws IOException {
        return dataInputStream.readChar();
    }

    @Override
    public int readInt() throws IOException {
        return dataInputStream.readInt();
    }

    @Override
    public long readLong() throws IOException {
        return dataInputStream.readLong();
    }

    @Override
    public float readFloat() throws IOException {
        return dataInputStream.readFloat();
    }

    @Override
    public double readDouble() throws IOException {
        return dataInputStream.readDouble();
    }

    @Override
    public String readLine() throws IOException {
        throw new IOException("Not supported");
    }

    @Override
    public String readUTF() throws IOException {
        return dataInputStream.readUTF();
    }

}
