package com.wisdom.collect.modbus.io;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;


public class BytesOutputStream extends FastByteArrayOutputStream implements DataOutput {

    private DataOutputStream dataOutputStream;



    public BytesOutputStream(int size) {
        super(size);
        dataOutputStream = new DataOutputStream(this);
    }


    public BytesOutputStream(byte[] buffer) {
        buf = buffer;
        count = 0;
        dataOutputStream = new DataOutputStream(this);
    }

    public synchronized byte[] getBuffer() {
        byte[] dest = new byte[buf.length];
        System.arraycopy(buf, 0, dest, 0, dest.length);
        return dest;
    }

    @Override
    public void reset() {
        count = 0;
    }

    @Override
    public void writeBoolean(boolean v) throws IOException {
        dataOutputStream.writeBoolean(v);
    }

    @Override
    public void writeByte(int v) throws IOException {
        dataOutputStream.writeByte(v);
    }

    @Override
    public void writeShort(int v) throws IOException {
        dataOutputStream.writeShort(v);
    }

    @Override
    public void writeChar(int v) throws IOException {
        dataOutputStream.writeChar(v);
    }

    @Override
    public void writeInt(int v) throws IOException {
        dataOutputStream.writeInt(v);
    }

    @Override
    public void writeLong(long v) throws IOException {
        dataOutputStream.writeLong(v);
    }

    @Override
    public void writeFloat(float v) throws IOException {
        dataOutputStream.writeFloat(v);
    }

    @Override
    public void writeDouble(double v) throws IOException {
        dataOutputStream.writeDouble(v);
    }

    @Override
    public void writeBytes(String s) throws IOException {
        int len = s.length();
        for (int i = 0; i < len; i++) {
            this.write((byte)s.charAt(i));
        }
    }

    @Override
    public void writeChars(String s) throws IOException {
        dataOutputStream.writeChars(s);
    }

    @Override
    public void writeUTF(String str) throws IOException {
        dataOutputStream.writeUTF(str);
    }

}