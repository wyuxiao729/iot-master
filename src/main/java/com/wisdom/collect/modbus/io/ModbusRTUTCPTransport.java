package com.wisdom.collect.modbus.io;


import com.wisdom.collect.modbus.ModbusIOException;
import com.wisdom.collect.modbus.msg.ModbusRequest;
import com.wisdom.collect.modbus.msg.ModbusResponse;

import java.net.Socket;


public class ModbusRTUTCPTransport extends ModbusTCPTransport {

    public ModbusRTUTCPTransport() {
        // RTU over TCP is headless by default
        setHeadless();
    }

    public ModbusRTUTCPTransport(Socket socket) {
        super(socket);
        // RTU over TCP is headless by default
        setHeadless();
    }

    @Override
    public void writeResponse(ModbusResponse msg) throws ModbusIOException {
        writeMessage(msg, true);
    }

    @Override
    public void writeRequest(ModbusRequest msg) throws ModbusIOException {
        writeMessage(msg, true);
    }
}
