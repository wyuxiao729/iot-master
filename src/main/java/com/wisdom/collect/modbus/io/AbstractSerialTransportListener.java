package com.wisdom.collect.modbus.io;

import com.wisdom.collect.modbus.msg.ModbusMessage;
import com.wisdom.collect.modbus.msg.ModbusRequest;
import com.wisdom.collect.modbus.msg.ModbusResponse;
import com.wisdom.collect.modbus.net.AbstractSerialConnection;

abstract public class AbstractSerialTransportListener {


    public void beforeMessageWrite(AbstractSerialConnection port, ModbusMessage msg) {
    }


    public void afterMessageWrite(AbstractSerialConnection port, ModbusMessage msg) {
    }


    public void beforeRequestRead(AbstractSerialConnection port) {
    }


    public void afterRequestRead(AbstractSerialConnection port, ModbusRequest req) {
    }


    public void beforeResponseRead(AbstractSerialConnection port) {
    }


    public void afterResponseRead(AbstractSerialConnection port, ModbusResponse res) {
    }
}
