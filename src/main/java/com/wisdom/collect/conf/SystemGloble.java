package com.wisdom.collect.conf;

import java.util.HashMap;
import java.util.Map;

public class SystemGloble {
	
	public static final String GRANFANA_URL = "GRANFANA_URL";
	public static final String COLLECTOR_CODE = "COLLECTOR_CODE";
	public static final String MOCK_FLAG = "MOCK_FLAG";
	
	public static final String IS_MOCK = "mock";

	private static Map<String, String> param = new HashMap<>();
	
	public static String getParam(String key) {
		return param.get(key);
	}
	
	public static void setParam(String key, String value) {
		param.put(key, value);
	}

}
