package com.wisdom.collect.collector;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class CollectorServer extends Thread {
	private static Logger logger = Logger.getLogger(CollectorServer.class);
	private static CollectorServer server;
	private ServerSocket serverSocket;
	private final int POOL_SIZE = 10;
	private int port;


	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port
	 *            the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	private CollectorServer() {
	}

	public static CollectorServer getInstance() {
		if (null == server) {
			server = new CollectorServer();
		}
		return server;
	}

	private void init(int port) throws IOException {
		serverSocket = new ServerSocket(port);
		logger.debug("-------------------采集服务已启动----------------");
	}

	private void startExecute() {
		while (true) {
			Socket socket = null;
			try {
				socket = serverSocket.accept();
				logger.debug("-------------------监听到新的客户端连接----------------");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 入口方法<br>
	 * 一个核心mis只有一个ip和端口
	 * 
	 */
	@Override
	public void run() {

		try {
			server.init(port);
			server.startExecute();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
